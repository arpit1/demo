package com.wudstay.tms.model;


import com.wudstay.tms.pojo.BasePojo;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("getAllPgCities.do")
    Call<BasePojo> getPGCities();

}
