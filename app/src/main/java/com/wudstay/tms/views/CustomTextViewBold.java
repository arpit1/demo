package com.wudstay.tms.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;


/**
 * Created by arpit on 25-Nov-15.
 */
public class CustomTextViewBold extends TextView {
    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Semibold.ttf");
        this.setTypeface(face);
    }
}
