package com.wudstay.tms.views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditText extends EditText {
	public CustomEditText(Context context, AttributeSet attrs) {
		super(context, attrs);
		Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/Lato-Light.ttf");
		this.setTypeface(face);
	}
}
