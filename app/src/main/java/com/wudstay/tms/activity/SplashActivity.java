package com.wudstay.tms.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.wudstay.tms.R;

public class SplashActivity extends BaseActivity {

    private Handler splashTimeHandler;
    private Runnable finalizer;
    private SplashActivity ctx = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        splashTimeHandler = new Handler();
        finalizer = new Runnable() {
            public void run() {
                Intent mainIntent = new Intent(ctx, HomeActivity.class);
                startActivity(mainIntent);
                finish();
            }
        };
        splashTimeHandler.postDelayed(finalizer, 3000);
    }
}
