package com.wudstay.tms.utils;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by arpit on 1/5/2017.
 */

public class WudStayConstants {

    public final static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public final static String PREF_NAME = "com.wudstaytms.prefs";
    public final static String DEFAULT_VALUE = "";
    //    public final static String BASE_URL = "http://54.251.116.126:8080/wudstay/webservice/";
    public final static String BASE_URL = "http://www.wudstay.com/webservice/";
    public final static String NO_INTERNET_CONNECTED = "No internet connection available";
}
